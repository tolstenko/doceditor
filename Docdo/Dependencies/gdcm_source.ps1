$gdcmzipfile = "$PSScriptRoot\GDCM-2.8.4.zip"
if(![System.IO.File]::Exists($gdcmzipfile)){
    $url = "https://github.com/malaterre/GDCM/archive/v2.8.4.zip"
	$start_time = Get-Date
	$wc = New-Object System.Net.WebClient
	$wc.DownloadFile($url, $gdcmzipfile)
	Write-Output "Time taken downloading: $((Get-Date).Subtract($start_time).Seconds) second(s)"
}

Write-Host "psfolder: $PSScriptRoot"

$gdcmfolder = "$PSScriptRoot\GDCM-SOURCE"
if(!(Test-Path -Path $gdcmfolder )){
    $start_time = Get-Date
    Add-Type -AssemblyName System.IO.Compression.FileSystem
    [System.IO.Compression.ZipFile]::ExtractToDirectory($gdcmzipfile, "$PSScriptRoot\")
    Write-Output "Time taken unziping: $((Get-Date).Subtract($start_time).Seconds) second(s)"
    ren "$PSScriptRoot\GDCM-2.8.4" "$PSScriptRoot\GDCM-SOURCE"
}