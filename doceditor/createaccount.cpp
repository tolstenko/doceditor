#include "createaccount.h"
#include "ui_createaccount.h"
#include "createexamdialog.h"

createaccount::createaccount(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::createaccount)
{
    ui->setupUi(this);


    // Logo DocDo
    QPixmap pix("://resources/images/DocDoLogo.png");
    ui->logo->setPixmap(pix.scaled(200,200,Qt::KeepAspectRatio));
    ui->logo->setAlignment(Qt::AlignCenter);
    //ui->logo->setMargin(50);

    setWindowTitle( tr("Create Account") );
    //setModal( true );
    connect(manager3, SIGNAL(finished(QNetworkReply *)), this, SLOT(replyFinished3(QNetworkReply*)));

    // Background
    QPixmap pixmap;
    pixmap.load("://resources/images/Backgorund.png");
    //scaling the image, optional. See the documentation for more options
    pixmap = pixmap.scaled(345,800,Qt::KeepAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Window, pixmap);
    this->setPalette(palette);


    // Button Register
    ui->loginPushButtonNewAccount->setStyleSheet("background-color: rgb(0,39,69); color:white");
    ui->loginPushButtonNewAccount->setMinimumHeight(40);


    ui->nameInputNewAccount->setStyleSheet("padding: 0 8px;");
    ui->passwordInputNewAccount->setStyleSheet("padding: 0 8px;");
    ui->emailInputNewAccount_3->setStyleSheet("padding: 0 8px;");




    ui->nameNewAccount->setStyleSheet("color:white");
    ui->passwordNewAccount->setStyleSheet("color:white");
    ui->emailNewAccount->setStyleSheet("color:white");
    ui->typeNewAccount->setStyleSheet("color:white");


    ui->typeComboNewAccount->addItem("Doctor");
    ui->typeComboNewAccount->addItem("Patient");



}

createaccount::~createaccount()
{
    delete ui;
}

void createaccount::on_loginPushButtonNewAccount_clicked()
{

    QNetworkRequest req = QNetworkRequest(QUrl("https://www.docdo.com.br/v3/process/api.php"));
    req.setHeader(req.ContentTypeHeader,"application/x-www-form-urlencoded");

    QString postString = "api=register&login=" + ui->emailInputNewAccount_3->text() +"&name="+ui->nameInputNewAccount->text() +"&password="+ui->passwordInputNewAccount->text() +"&email="+ui->emailInputNewAccount_3->text() +"&category=\'[\""+ui->typeComboNewAccount->currentText()+"\"]\'";
    manager3->post(req,postString.toUtf8());

    qDebug() << "CREATE ACCOUNT";

}



void createaccount::replyFinished3(QNetworkReply *networkReply)
{
    QString response = networkReply->readAll();
    qDebug() << "Reply CreateAccount:" << response;
    if(response.isEmpty()){
        //ui->status->setText("no response from server");
        return;
    }
    QJsonDocument doc = QJsonDocument::fromJson(response.toUtf8());
    if(!doc.isObject())
    {
        //ui->status->setText("received invalid response from server");
        return;
    }

    QJsonObject json = doc.object();
    QString error = json["error"].toString();
    if(!error.isEmpty()&&!error.isNull())
    {
        //ui->status->setText(error);
        return;
    }

    QJsonObject data = json["data"].toObject();
    if(data.isEmpty())
    {
        //ui->status->setText("data response from server is empty");
        return;
    }



    CreateExamDialog *cr = new CreateExamDialog();
    cr->show();


    this->close();
}












