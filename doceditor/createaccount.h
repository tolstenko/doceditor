#ifndef CREATEACCOUNT_H
#define CREATEACCOUNT_H

#include <QWidget>

#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QComboBox>
#include <QGridLayout>
#include <QStringList>
#include <QDebug>
#include <QString>
#include <QNetworkReply>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QList>

namespace Ui {
class createaccount;
}

class createaccount : public QWidget
{
    Q_OBJECT

public:
    explicit createaccount(QWidget *parent = 0);
    ~createaccount();

public slots:
    void replyFinished3(QNetworkReply *networkReply);
private slots:
    void on_loginPushButtonNewAccount_clicked();

private:
    Ui::createaccount *ui;
     QNetworkAccessManager *manager3 = new QNetworkAccessManager(this);
};

#endif // CREATEACCOUNT_H
