#include "createexamdialog.h"
#include "ui_createexamdialog.h"
#include  "dicomdb.h"
#include "uploadarquivo.h"



CreateExamDialog::CreateExamDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateExamDialog)
{
    ui->setupUi(this);
    ui->comboBox->addItem("Tomografia");
    ui->comboBox->addItem("Ressonância");
    ui->comboBox->addItem("Ultrassom");
    connect(manager, SIGNAL(finished(QNetworkReply *)), this, SLOT(reply(QNetworkReply*)));

}

CreateExamDialog::~CreateExamDialog()
{
    delete ui;
}

void CreateExamDialog::on_pushButton_clicked()
{
    ui->status->setText("trying");


         QNetworkRequest req = QNetworkRequest(QUrl("https://www.docdo.com.br/v3/process/api.php"));
         req.setHeader(req.ContentTypeHeader,"application/x-www-form-urlencoded");
         QString postString = "api=createexam&token=" + DicomDB::instance()->token +"&type="+ui->comboBox->currentText()+"&patient_id="+DicomDB::instance()->id+"&description="+ui->lineEdit_2->text();
         manager->post(req,postString.toUtf8());

         p.show();//Progress bar


}

void CreateExamDialog::reply(QNetworkReply *networkReply){

    QString response = networkReply->readAll();
    qDebug() << "Reply:" << response;

    if(response.isEmpty()){
        ui->status->setText("no response from server");
        return;
    }
    QJsonDocument doc = QJsonDocument::fromJson(response.toUtf8());
    if(!doc.isObject())
    {
        ui->status->setText("received invalid response from server");
        return;
    }

    QJsonObject json = doc.object();
    QString error = json["error"].toString();
    if(!error.isEmpty()&&!error.isNull())
    {
        ui->status->setText(error);
        return;
    }

    QJsonObject data = json["data"].toObject();
    if(data.isEmpty())
    {
        ui->status->setText("data response from server is empty");
        return;
    }

    DicomDB::instance()->exam_id = data["exam_id"].toString();
    UploadArquivo *up= new UploadArquivo();
                 up->show();
                 p.close();//Progress bar
                 this->close();


}
