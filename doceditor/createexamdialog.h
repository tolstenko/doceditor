#ifndef CREATEEXAMDIALOG_H
#define CREATEEXAMDIALOG_H

#include <QDialog>
#include <QDebug>
#include <QString>
#include <QNetworkReply>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QList>
#include <QNetworkAccessManager>
#include "progresswindow.h"

namespace Ui {
class CreateExamDialog;
}

class CreateExamDialog : public QDialog
{
    Q_OBJECT

private:
    Ui::CreateExamDialog *ui;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

public:
    explicit CreateExamDialog(QWidget *parent = 0);
    ~CreateExamDialog();
    progresswindow p;

public slots:

    void reply(QNetworkReply *networkReply);

private slots:
    void on_pushButton_clicked();


};

#endif // CREATEEXAMDIALOG_H
