#include "dicombrowser.h"
#include "ui_dicombrowser.h"

DicomBrowser::DicomBrowser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DicomBrowser)
{
    ui->setupUi(this);
    patientsmodel = new QSqlQueryModel();
    studiesmodel = new  QSqlQueryModel();
    seriesmodel = new QSqlQueryModel();
    patientsquery = new QSqlQuery(DicomDB::instance()->db);
    studiesquery = new QSqlQuery(DicomDB::instance()->db);
    seriesquery = new QSqlQuery(DicomDB::instance()->db);
    loadquery = new QSqlQuery(DicomDB::instance()->db);
    UpdateTablePatients();
}

DicomBrowser::~DicomBrowser()
{
    delete patientsmodel;
    delete studiesmodel;
    delete seriesmodel;
    delete patientsquery;
    delete studiesquery;
    delete seriesquery;
    delete ui;
}

void DicomBrowser::on_scandirectory_clicked()
{
    const QString DEFAULT_DIR_KEY("default_dir");
    QSettings settings(QSettings::NativeFormat,QSettings::UserScope,"InfiniBrains","InfiniEditor");

    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                  settings.value(DEFAULT_DIR_KEY).toString(),
                  QFileDialog::ShowDirsOnly
                  | QFileDialog::DontResolveSymlinks);

    // navigate through folders recursivelly if dir is set
    if(!dir.isEmpty())
    {
        settings.setValue(DEFAULT_DIR_KEY,dir);
        DicomDB::instance()->navigate(dir);

        //FolderCompressor *compressor = new FolderCompressor();
        //QDir parent = QDir(dir);
        //parent.cdUp();
       // compressor->compressFolder(dir,parent.path()+"/compressed.zip");
        //delete compressor;
    }
    else
        return;
    this->UpdateTablePatients();
}

void DicomBrowser::UpdateTableSeries()
{
   /* QItemSelectionModel *select = ui->tableViewStudies->selectionModel();
    if(select->hasSelection())//check if has selection
    {
        QString studyid = select->selectedRows().at(0).sibling(0,0).data().toString();
        seriesquery->prepare("SELECT DISTINCT SeriesNumber, SeriesDate, SeriesTime, SeriesDescription, Modality, BodyPartExamined, AcquisitionNumber, ContrastAgent, ScanningSequence, EchoNumber, TemporalPosition FROM dicoms WHERE StudyID = '" + studyid +"' ;");
        seriesquery->exec();
        seriesmodel->setQuery(*seriesquery);
        ui->tableViewSeries->setModel(seriesmodel);
        ui->tableViewSeries->verticalHeader()->hide();
        ui->tableViewSeries->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
        ui->tableViewSeries->setSelectionBehavior(QAbstractItemView::SelectRows);
    }
    else
    {*/
        //QString studyid = select->selectedRows().at(0).sibling(0,0).data().toString();
        seriesquery->prepare("SELECT DISTINCT SeriesNumber, SeriesDate, SeriesTime, SeriesDescription, Modality, BodyPartExamined, AcquisitionNumber, ContrastAgent, ScanningSequence, EchoNumber, TemporalPosition FROM dicoms");
        seriesquery->exec();
        seriesmodel->setQuery(*seriesquery);
        ui->tableViewSeries->setModel(seriesmodel);
        ui->tableViewSeries->verticalHeader()->hide();
        ui->tableViewSeries->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
        ui->tableViewSeries->setSelectionBehavior(QAbstractItemView::SelectRows);
    //}

   // if (ui->tableViewSeries->model()->rowCount()>0)
    //    ui->tableViewSeries->selectRow(0);
}

void DicomBrowser::UpdateTableStudies()
{
   /* QItemSelectionModel *select = ui->tableViewPatients->selectionModel();
    if(select->hasSelection())//check if has selection
    {
        QString patientname = select->selectedRows().at(0).sibling(0,0).data().toString();
        studiesquery->prepare("SELECT DISTINCT StudyID, StudyDate, StudyTime, AccessionNumber, ModalitiesInStudy, InstitutionName, ReferringPhysician, PerformingPhysicianName, StudyDescription FROM dicoms WHERE PatientsName = '" + patientname +"' ;");
        studiesquery->exec();
        studiesmodel->setQuery(*studiesquery);
        ui->tableViewStudies->setModel(studiesmodel);
        ui->tableViewStudies->verticalHeader()->hide();
        ui->tableViewStudies->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
        ui->tableViewStudies->setSelectionBehavior(QAbstractItemView::SelectRows);
    }
    else
    {*/
       // QString patientname = select->selectedRows().at(0).sibling(0,0).data().toString();
        studiesquery->prepare("SELECT DISTINCT StudyID, StudyDate, StudyTime, AccessionNumber, ModalitiesInStudy, InstitutionName, ReferringPhysician, PerformingPhysicianName, StudyDescription FROM dicoms");
        studiesquery->exec();
        studiesmodel->setQuery(*studiesquery);
        ui->tableViewStudies->setModel(studiesmodel);
        ui->tableViewStudies->verticalHeader()->hide();
        ui->tableViewStudies->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
        ui->tableViewStudies->setSelectionBehavior(QAbstractItemView::SelectRows);
   // }

   // if (ui->tableViewStudies->model()->rowCount()>0)
     //   ui->tableViewStudies->selectRow(0);

    UpdateTableSeries();
}

void DicomBrowser::UpdateTablePatients()
{
    // patient
    patientsquery->prepare("SELECT DISTINCT PatientsName, PatientID, PatientsBirthDate, PatientsBirthTime, PatientsSex, PatientsAge, PatientComments FROM dicoms");
    patientsquery->exec();
    patientsmodel->setQuery(*patientsquery);
    ui->tableViewPatients->setModel(patientsmodel);
    ui->tableViewPatients->verticalHeader()->hide();
    ui->tableViewPatients->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    ui->tableViewPatients->setSelectionBehavior(QAbstractItemView::SelectRows);

    // select first row
   // if (ui->tableViewPatients->model()->rowCount()>0)
     //   ui->tableViewPatients->selectRow(0);

    UpdateTableStudies();
}

void DicomBrowser::on_pushButton_clicked()
{
    this->close();
}

void DicomBrowser::on_load_clicked()
{
    QItemSelectionModel *patientSelection = ui->tableViewPatients->selectionModel();
    QItemSelectionModel *studySelection   = ui->tableViewStudies->selectionModel();
    QItemSelectionModel *seriesSelection  = ui->tableViewSeries->selectionModel();

    //check if has selection
    if(patientSelection->hasSelection() && studySelection->hasSelection() && seriesSelection->hasSelection())
    {
        QString patientname = patientSelection->selectedRows().at(0).sibling(0,0).data().toString();
        QString studyid = studySelection->selectedRows().at(0).sibling(0,0).data().toString();
        QString seriesnumber = seriesSelection->selectedRows().at(0).sibling(0,0).data().toString();

        DicomDB::instance()->instantiateVolume(patientname,studyid,seriesnumber);
        this->close();
    }
}

void DicomBrowser::on_tableViewPatients_clicked(const QModelIndex &index)
{
    UpdateTableStudies();
}

void DicomBrowser::on_tableViewStudies_clicked(const QModelIndex &index)
{
    UpdateTableSeries();
}
