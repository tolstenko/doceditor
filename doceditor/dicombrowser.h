#ifndef DICOMBROWSER_H
#define DICOMBROWSER_H

#include "dicomdb.h"
#include "mainwindow.h"
#include "foldercompressor.h"

#include <QMainWindow>
#include <QString>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QAbstractItemView>
#include <QSettings>
#include <QDebug>
#include <QFileDialog>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlDatabase>
#include <QtSql/QtSql>
#include <QTableWidgetItem>

namespace Ui {
class DicomBrowser;
}

class DicomBrowser : public QDialog
{
    Q_OBJECT

public:
    explicit DicomBrowser(QWidget *parent = 0);
    ~DicomBrowser();

private slots:
    void on_load_clicked();
    void on_scandirectory_clicked();
    void on_pushButton_clicked();
    void on_tableViewPatients_clicked(const QModelIndex &index);
    void on_tableViewStudies_clicked(const QModelIndex &index);


private:
    void UpdateTablePatients();
    void UpdateTableStudies();
    void UpdateTableSeries();

    Ui::DicomBrowser *ui;
    QSqlQueryModel * patientsmodel;
    QSqlQueryModel * studiesmodel;
    QSqlQueryModel * seriesmodel;
    QSqlQuery * patientsquery;
    QSqlQuery * studiesquery;
    QSqlQuery * seriesquery;
    QSqlQuery * loadquery;
};

#endif // DICOMBROWSER_H
