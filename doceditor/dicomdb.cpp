#include "dicomdb.h"
#include "qcryptographichash.h"

/*
 * Patient Position (0018, 5100) will tell you if the patient was scanned head-first supine,
 * feet-first prone, head-first prone, etc. Instance Number (0020, 0013), also commonly known
 * as slice number, contains no information about spatial location and isn't even guaranteed
 * to be unique. Slice Location (0020, 1041) is useful, if it exists, but you can't count on
 * it always existing because it's a Type 3 (optional) attribute. To have a robust solution,
 * you need to use Image Position Patient (0020, 0032) together with Image Orientation
 * Patient (0020, 0037) and Patient Position (0018, 5100) to properly order the slices in
 * space. Image Position Patient gives you a vector from the origin to the center of the
 * first transmitted pixel of the image. Image Orientation Patient gives you vectors for the
 * orientation of the rows and columns of the image in space. Patient Position tells you how
 * the patient was placed on the table relative to the coordinate system.
 */

DicomDB::DicomDB()
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    QString folder = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);

    if(!QDir(folder).exists())
        QDir().mkdir(folder);



    QString path = folder + "/docdo.db";
    db.setDatabaseName(path);

    qDebug() << path;

    if (!db.open())
       qDebug() << "Error: connection with database fail";
    else
       qDebug() << "Database: connection ok";
    QSqlQuery query;
    query.prepare("CREATE TABLE IF NOT EXISTS credentials (id TEXT, login TEXT, token TEXT, language TEXT, category TEXT);");
    if(!query.exec())
        qDebug() << "Creating table error: " << query.lastError();
    query.prepare("CREATE TABLE IF NOT EXISTS dicoms ( path TEXT PRIMARY KEY, PatientsName TEXT, PatientID TEXT, PatientsBirthDate TEXT, PatientsBirthTime TEXT, PatientsSex TEXT, PatientsAge TEXT, PatientComments TEXT, StudyID TEXT, StudyDate TEXT, StudyTime TEXT, AccessionNumber TEXT, ModalitiesInStudy TEXT, InstitutionName TEXT, ReferringPhysician TEXT, PerformingPhysicianName TEXT, StudyDescription TEXT, SeriesNumber Text, SeriesDate Text, SeriesTime Text, SeriesDescription Text, Modality Text, BodyPartExamined Text, AcquisitionNumber Text, ContrastAgent Text, ScanningSequence Text, EchoNumber Text, TemporalPosition Text);");
    if(!query.exec())
        qDebug() << "Creating table error: " << query.lastError();
}

// Global static pointer used to ensure a single instance of the class.
DicomDB* DicomDB::_instance = nullptr;

// This function is called to create an instance of the class. Calling the constructor publicly is not allowed. The constructor is private and is only called by this Instance function.
DicomDB* DicomDB::instance()
{
   if (!_instance)   // Only allow one instance of class to be generated.
   {
      _instance = new DicomDB;
   }
   return _instance;
}

QString DicomDB::dicomTagToString(gdcm::Tag tag)
{
    const gdcm::Global& g = gdcm::Global::GetInstance();
    const gdcm::Dicts &dicts = g.GetDicts();
    const gdcm::Dict &pubdict = dicts.GetPublicDict();
    const gdcm::PrivateDict &privdict = dicts.GetPrivateDict();

    gdcm::DictEntry ent = pubdict.GetDictEntry(tag);

    QString ret;
    if (ent.GetVR() != gdcm::VR::INVALID )
        ret= QString::fromStdString(ent.GetName()).simplified();
    if(ret.isEmpty())
    {
        ent = privdict.GetDictEntry(tag);
        if (ent.GetVR() != gdcm::VR::INVALID )
            ret = QString::fromStdString(ent.GetName()).simplified();
    }
    if(ret.isEmpty())
        ret = "x" + QString("%1").arg(tag.GetElementTag(), 8, 16, QLatin1Char( '0' ));
    ret.replace(" ","");
    ret.replace("'","");
    ret.replace("(","");
    ret.replace(")","");
    ret.replace(",","");
    ret.replace("-","");
    ret.replace("/","");
    return ret;
}

void DicomDB::instantiateVolume(QString patientname,QString study, QString serie)
{
    QSqlQuery loadquery;
    loadquery.prepare("SELECT path, InstanceNumber FROM dicoms WHERE PatientsName = '" + patientname+ "' AND StudyID = '" + study +"' AND SeriesNumber = '"+serie +"'; ");
    if(!loadquery.exec())
        qDebug() << "selecting error: " << loadquery.lastError();
    else
    {
        // sort instances
        QMap<int,QString> map;
        while (loadquery.next())
        {
            QString path = loadquery.value(0).toString();
            int instance = loadquery.value(1).toString().toInt();
            map[instance] = path;
        }
        data.clear();

        // load each instance into a volume
        foreach(int i, map.keys())
        {
            ImageReader imgreader;
            imgreader.SetFileName(map[i].toLatin1().data());
            if(!imgreader.Read())
            {
                qDebug () << "dicom not readable: " << map[i];
                continue;
            }
            // get image
            Image img = imgreader.GetImage();
            char *buffer = new char[img.GetBufferLength()];
            pixelformat = img.GetPixelFormat();
            img.GetBuffer(buffer);

            // store slices into volume data
            for(int j=0;j<img.GetBufferLength();j++)
                data.push_back(buffer[j]);
            delete buffer;
        }
    }
}

void DicomDB::navigate(QString dir)
{
    QDirIterator it(dir, QStringList() << "*", QDir::Files, QDirIterator::Subdirectories);

    QMimeDatabase mimedb;
    QStringList mimeTypeFilters;

    mimeTypeFilters << "image/jpeg" << "image/pjpeg" << "image/png" << "image/gif"
                    << "image/x-portable-bitmap" << "image/x-portable-graymap"
                    << "application/dicom";

    // while there is new file
    while (it.hasNext())
    {
        QString path = it.next();

        // check if the file is already imported
        QString fileQueryStr = "SELECT path FROM dicoms WHERE path = '" + path + "';";
        //qDebug () << fileQueryStr;
        QSqlQuery fileQuery;
        fileQuery.prepare(fileQueryStr);
        if(fileQuery.exec())
        {
            if(fileQuery.next() && !fileQuery.value(0).toString().isEmpty())
            {
                qDebug() <<"file already in the database: " << path;
                continue;
            }
        }

        // check mime type
        QMimeType mime = mimedb.mimeTypeForFile(path);

        // check if it is supported
        if(!mimeTypeFilters.contains(mime.name()))
        {
            //qDebug()<<mime.name() << path;
            continue;
        }
        // check if it is dicom
        if(mime.inherits("application/dicom"))
        {
            qDebug() << "Dicom: " << mime.name() << path;

            // read dicom data
            gdcm::Reader reader;
            reader.SetFileName(path.toUtf8().constData());
            if(!reader.Read())
                continue;
            gdcm::File &file = reader.GetFile();
            gdcm::DataSet &ds = file.GetDataSet();
            gdcm::StringFilter sf;
            sf.SetFile(file);



//+++++++++++++++++++++++++++++++++++++++++++++++++ SHA1 Criptography Hash ++++++++++++++++++///


            std::string s = sf.ToString(gdcm::Tag(0x0010,0x0010));
            std::string c = sf.ToString(gdcm::Tag(0x0010,0x1002));
             std::string a = sf.ToString(gdcm::Tag(0x0020,0x0010));
            QString tagVal3 = a.c_str();
             QString tagVal2 = c.c_str();
            QString tagVal = s.c_str();
                    tagVal = tagVal.trimmed();
            QByteArray hash = QCryptographicHash::hash(tagVal.toUtf8(), QCryptographicHash::Sha1);
            QString newTagVal = hash.toHex().toUpper();
            //qDebug("Replacing %s with %s",tagVal.toStdString().c_str(),newTagVal.toStdString().c_str());
            sha1 = newTagVal.toStdString().c_str();
            seq_id= tagVal2.toUtf8();
            study = tagVal3.toUtf8();
            //+++++++++++++++++++++++++++++++++++++++++++++++++ SHA1 Criptography Hash ++++++++++++++++++///



            // store all key value from dicom
            QList<QString>keys,values;
            keys.push_back("path");
            values.push_back(path);
            for(std::set<gdcm::DataElement>::const_iterator it = ds.Begin();it!=ds.End();++it)
            {
                QString value = QString::fromUtf8(sf.ToString( it->GetTag() ).c_str());
                QString key = dicomTagToString(it->GetTag());
                qDebug () << key;
                if(!value.isEmpty())
                {
                    keys.push_back(key);
                    values.push_back(value);
                }
            }

            // prepare query to insert data into db
            QString insertQuery = "INSERT INTO dicoms (";
            for(int i=0;i<keys.length()-1;i++)
                insertQuery.append(keys[i]+ ",");
            insertQuery.append(keys[keys.length()-1]+ ")");

            insertQuery.append(" VALUES (");
            for(int i=0;i<values.length()-1;i++)
                insertQuery.append("'"+values[i]+ "' ,");
            insertQuery.append("'"+values[keys.length()-1]+ "' );");

            QSqlQuery query;
            query.prepare(insertQuery);

            // check if there are missing columns
            if(!query.exec())
            {
                // list all columns
                query.prepare("PRAGMA table_info('dicoms');");
                if(!query.exec())
                    qDebug() << "pragma error: " << query.lastError();

                // find missing columns. Add first, remove then. The remains is what we need to add.
                QList<QString> missingColumns;
                foreach (QString col, keys)
                    missingColumns.push_back(col);
                while(query.next())
                    missingColumns.removeOne(query.value(1).toString());

                // add columns
                foreach (QString m, missingColumns) {
                    QString alterQueryStr = "ALTER TABLE dicoms ADD COLUMN "+ m +" TEXT";
                    query.prepare(alterQueryStr);
                    if(!query.exec())
                        qDebug() << "ALTER TABLE error: " << query.lastError() << alterQueryStr;
                }

                // try to insert it again
                query.prepare(insertQuery);
                if(!query.exec())
                     qDebug() << "INSERT error: " << query.lastError();
            }

            /*
            // find pacient;
            QList <QString> dicomData;
            QString name;
            if(ds.FindDataElement(patientnameTag))
            {
                dicomData.append( QString::fromUtf8(sf.ToString( patientnameTag ).c_str()).simplified() );
                name = QString::fromUtf8(sf.ToString( patientnameTag ).c_str()).simplified();
            }else
            {
                dicomData.append("null");
            }

            if(ds.FindDataElement(patientidTag))
            {
                dicomData.append( QString::fromUtf8(sf.ToString( patientidTag ).c_str()).simplified() );
            }else
            {
                dicomData.append("null");
            }

            if(ds.FindDataElement(patientbirthdateTag))
            {
                dicomData.append( QString::fromUtf8(sf.ToString( patientbirthdateTag ).c_str()).simplified() );
            }else
            {
                dicomData.append("null");
            }

            if(ds.FindDataElement(patientbirthtimeTag))
            {
                dicomData.append( QString::fromUtf8(sf.ToString( patientbirthtimeTag ).c_str()).simplified() );
            }else
            {
                dicomData.append("null");
            }

            if(ds.FindDataElement(patientcommentsTag))
            {
                dicomData.append( QString::fromUtf8(sf.ToString( patientcommentsTag ).c_str()).simplified() );
            }else
            {
                dicomData.append("null");
            }

            if(ds.FindDataElement(patientsexTag))
            {
                dicomData.append( QString::fromUtf8(sf.ToString( patientsexTag ).c_str()).simplified() );
            }else
            {
                dicomData.append("null");
            }



            // find study
            QString study;
            if(ds.FindDataElement(studyidTag))
            {
                dicomData.append( QString::fromUtf8(sf.ToString(studyidTag).c_str()).simplified() );
                study = QString::fromUtf8(sf.ToString(studyidTag).c_str()).simplified();
            }else{
                dicomData.append( "null" );
                study = "null";
            }

            if(ds.FindDataElement(studydateTag))
            {
                dicomData.append( QString::fromUtf8(sf.ToString(studydateTag).c_str()).simplified() );
            }else{
                dicomData.append("null");
            }

            if(ds.FindDataElement(studytimeTag))
            {
                dicomData.append( QString::fromUtf8(sf.ToString(studytimeTag).c_str()).simplified() );
            }else{
                dicomData.append("null");
            }

            if(ds.FindDataElement(studydescriptionTag))
            {
                dicomData.append( QString::fromUtf8(sf.ToString(studydescriptionTag).c_str()).simplified() );
            }else{
                dicomData.append("null");
            }

            if(ds.FindDataElement(accessionnumberTag))
            {
                dicomData.append( QString::fromUtf8(sf.ToString(accessionnumberTag).c_str()).simplified() );
            }else{
                dicomData.append("null");
            }

            if(ds.FindDataElement(modalitiesinstudyTag))
            {
                dicomData.append( QString::fromUtf8(sf.ToString(modalitiesinstudyTag).c_str()).simplified() );
            }else{
                dicomData.append("null");
            }

            if(ds.FindDataElement(institutionnameTag))
            {
                dicomData.append( QString::fromUtf8(sf.ToString(institutionnameTag).c_str()).simplified() );
            }else{
                dicomData.append("null");
            }

            if(ds.FindDataElement(referringphysicianTag))
            {
                dicomData.append( QString::fromUtf8(sf.ToString(referringphysicianTag).c_str()).simplified() );
            }else{
                dicomData.append("null");
            }

            if(ds.FindDataElement(performingphysicianTag))
            {
                dicomData.append( QString::fromUtf8(sf.ToString(performingphysicianTag).c_str()).simplified() );
            }else{
                dicomData.append("null");
            }





            // find series
            QString serie;
            if(ds.FindDataElement(seriesnumberTag))
            {
                dicomData.append(QString::fromUtf8(sf.ToString(seriesnumberTag).c_str()).simplified());
                serie = QString::fromUtf8(sf.ToString(seriesnumberTag).c_str()).simplified();
            }else{
                dicomData.append( "null");
            }

            if(ds.FindDataElement(seriesdateTag))
            {
                dicomData.append(QString::fromUtf8(sf.ToString(seriesdateTag).c_str()).simplified());
            }else{
                dicomData.append( "null");
            }

            if(ds.FindDataElement(seriestimeTag))
            {
                dicomData.append(QString::fromUtf8(sf.ToString(seriestimeTag).c_str()).simplified());
            }else{
                dicomData.append( "null");
            }

            if(ds.FindDataElement(seriesdescriptionTag))
            {
                dicomData.append(QString::fromUtf8(sf.ToString(seriesdescriptionTag).c_str()).simplified());
            }else{
                dicomData.append( "null");
            }

            if(ds.FindDataElement(modalityTag))
            {
                dicomData.append(QString::fromUtf8(sf.ToString(modalityTag).c_str()).simplified());
            }else{
                dicomData.append( "null");
            }

            if(ds.FindDataElement(bodypartexaminedTag))
            {
                dicomData.append(QString::fromUtf8(sf.ToString(bodypartexaminedTag).c_str()).simplified());
            }else{
                dicomData.append( "null");
            }

            if(ds.FindDataElement(acquisitionnumberTag))
            {
                dicomData.append(QString::fromUtf8(sf.ToString(acquisitionnumberTag).c_str()).simplified());
            }else{
                dicomData.append( "null");
            }

            if(ds.FindDataElement(contrastagentTag))
            {
                dicomData.append(QString::fromUtf8(sf.ToString(contrastagentTag).c_str()).simplified());
            }else{
                dicomData.append( "null");
            }

            if(ds.FindDataElement(scanningsequenceTag))
            {
                dicomData.append(QString::fromUtf8(sf.ToString(scanningsequenceTag).c_str()).simplified());
            }else{
                dicomData.append( "null");
            }

            if(ds.FindDataElement(echonumberTag))
            {
                dicomData.append(QString::fromUtf8(sf.ToString(echonumberTag).c_str()).simplified());
            }else{
                dicomData.append( "null");
            }

            if(ds.FindDataElement(temporalpositionTag))
            {
                dicomData.append(QString::fromUtf8(sf.ToString(temporalpositionTag).c_str()).simplified());
            }else{
                dicomData.append( "null");
            }

            qDebug()<<dicomData;
            // find instance
            QString instance;
            if(ds.FindDataElement(instancenumberTag))
            {
                instance = QString::fromUtf8(sf.ToString(instancenumberTag).c_str()).simplified();
                //qDebug()<<instance;
            }
            //qDebug()<< "nome" + name;

            dicoms[name][study][serie][instance] = path;

            //if(!dicoms.contains(path))
            //    dicoms.push_back(path);
            */
        }
        else
        {
            //qDebug() << "Image: " << mime.name() << path;
            //if(!pictures.contains(path))
            //    pictures.push_back(path);
        }

        /*
        QStringList mimeTypeFilters;
        mimeTypeFilters << "image/jpeg" << "image/png";

        QFileDialog fd;
        fd.setFileMode(QFileDialog::ExistingFile);
        fd.setMimeTypeFilters(mimeTypeFilters);
        fd.exec();

        QString filename = fd.selectedFiles().count() == 1 ? fd.selectedFiles().at(0) : "";

        QMimeDatabase mimedb;
        if(!mimeTypeFilters.contains(mimedb.mimeTypeForFile(filename).name()))
        {
            // something is wrong here !
        }
        */
        /*gdcm::ImageReader reader;
        reader.SetFileName(it.next().toUtf8().constData());
        if(!reader.Read())
        {
            //QMessageBox::information(this,"File not read",it.next());
            continue;
        }*/

        //QImageReader reader(it.next());
        //if(reader.format()==)


        /*gdcm::File &file = reader.GetFile();
        gdcm::DataSet &ds = file.GetDataSet();
        const gdcm::Image &image = reader.GetImage();
        //QMessageBox::information(this,QString::number(image.GetRows()) + QString::number(image.GetColumns()),it.next());

        std::vector<char> vbuffer;
        vbuffer.resize( image.GetBufferLength() );
        char *buffer = &vbuffer[0];

        QImage *imageQt = NULL;
        if( !ConvertToFormat_RGB888( image, buffer, imageQt ) )
        {
            QMessageBox::information(this,"title","i couldnt create qimage from dcm");
        }
        else
        {
            QLabel *myLabel = new QLabel(this); // sets parent of label to main window
            myLabel->setPixmap(QPixmap::fromImage(*imageQt));
            myLabel->setScaledContents(true);
            myLabel->setFixedHeight(512);
            myLabel->setFixedWidth(512);

            myLabel->show();
        }
  // The output of gdcm::Reader is a gdcm::File
  //gdcm::File &file = reader.GetFile();

  // the dataset is the the set of element we are interested in:
  //gdcm::DataSet &ds = file.GetDataSet();

  const gdcm::Image &image = reader.GetImage();
  image.Print( std::cout );

  gdcm::ImageChangeTransferSyntax change;
  change.SetTransferSyntax( gdcm::TransferSyntax::JPEG2000Lossless );
  change.SetTransferSyntax( gdcm::TransferSyntax::JPEGLosslessProcess14_1 );
  //change.SetTransferSyntax( gdcm::TransferSyntax::JPEGBaselineProcess1 );
  //change.SetTransferSyntax( image.GetTransferSyntax() );
  change.SetInput( image );
  bool b = change.Change();
  if( !b )
    {
    std::cerr << "Could not change the Transfer Syntax" << std::endl;
    return 1;
    }

  //std::ofstream out( outfilename, std::ios::binary );
  //image.GetBuffer2(out);
  //out.close();
  gdcm::ImageWriter writer;
  writer.SetImage( change.GetOutput() );
  writer.SetFile( reader.GetFile() );
  writer.SetFileName( outfilename );
  if( !writer.Write() )
    {
    return 1;
    }

     */

    }
    //pictures.sort(Qt::CaseInsensitive);
    //dicoms.sort(Qt::CaseInsensitive);

  /*  QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");//not dbConnection
    qDebug() << name;
    db.setDatabaseName(QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation) + QDir::separator() + "docEditor.db");
    db.open();
    QSqlQuery query;
   bool qr = query.exec("create table person "
              "(id integer primary key, "
              "firstname varchar(20), "
              "lastname varchar(30), "
              "age integer)");
   if(qr){
       qDebug() << "tabela criada ";
   }else{
       qDebug() << query.lastError();
   }*/

}

