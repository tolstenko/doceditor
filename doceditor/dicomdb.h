#ifndef DICOMDB_H
#define DICOMDB_H

#include <QString>
#include <QStringList>
#include <QDir>
#include <QDirIterator>
#include <QImageReader>
#include <QMimeDatabase>
#include <QDebug>
#include <QMap>
#include <QString>
#include <QStandardPaths>
#include <QSqlDatabase>
#include <QtSql/QtSql>
#include <QSqlQuery>

#include <iterator>
#include <set>
#include <algorithm>

#include "gdcmReader.h"
#include "gdcmWriter.h"
#include "gdcmImage.h"
#include "gdcmImageReader.h"
#include "gdcmImageWriter.h"
#include "gdcmAttribute.h"
#include "gdcmImageChangeTransferSyntax.h"
#include "gdcmGlobal.h"
#include "gdcmDicts.h"
#include "gdcmDict.h"
#include "gdcmStringFilter.h"
#include "gdcmMediaStorage.h"

using namespace gdcm;

class DicomDB
{
public:
    static DicomDB* instance();
    void navigate(QString dir);
    void instantiateVolume(QString patient,QString study, QString serie);

    // folder, pictures
    QMap <QString, QStringList> pictures;

    QString id;
    QString login;
    QString token;
    QString language;
    QList<QString> category;
    QString signed_r;
    QString file_id;
    QString exam_id;
    QString sha1;
    QString seq_id;
    QString study;


    QSqlDatabase db;
private:
    DicomDB();
    //DicomDB(DicomDB const&){}             // copy constructor is private
    DicomDB& operator=(DicomDB const&){}  // assignment operator is private
    static DicomDB* _instance;
    QString dicomTagToString(gdcm::Tag tag);
    QVector<char> data;
    PixelFormat pixelformat;
};

#endif // DICOMDB_H
