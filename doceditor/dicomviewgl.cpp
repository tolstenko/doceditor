#include "dicomviewgl.h"

void DicomViewGL::initializeGL()
{
    // Set up the rendering context, load shaders and other resources, etc.:
    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
    f->glClearColor(1.0f, 0.0f, 1.0f, 1.0f);
}

void DicomViewGL::resizeGL(int w, int h)
{
    projection.setToIdentity();
    projection.perspective(45,((GLfloat)w)/((GLfloat)h),0.1f,1000.0f);
}

void DicomViewGL::paintGL()
{
    // Draw the scene:
    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
    f->glClear(GL_COLOR_BUFFER_BIT);
}
