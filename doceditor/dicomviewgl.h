#ifndef DICOMVIEWGL_H
#define DICOMVIEWGL_H

#include <QObject>
#include <QWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLShader>
#include <QOpenGLTexture>


class DicomViewGL : public QOpenGLWidget
{
    Q_OBJECT
public:
      DicomViewGL(QWidget *parent) : QOpenGLWidget(parent) { }
private:
      QOpenGLVertexArrayObject m_vao;
      QOpenGLBuffer m_vbo;
      QOpenGLShaderProgram *m_program;
      QOpenGLShader *m_shader;
      QOpenGLTexture *m_texture;
      QMatrix4x4 projection;

protected:
    void initializeGL();


    void resizeGL(int w, int h);

    void paintGL();

signals:

public slots:
};

#endif // DICOMVIEWGL_H
