#include "logindialog.h"
#include "ui_logindialog.h"
#include "createexamdialog.h"

LoginDialog::LoginDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::LoginDialog)
{


    ui->setupUi(this);

    // Logo DocDo
    QPixmap pix("://resources/images/DocDoLogo.png");
    ui->logo->setPixmap(pix.scaled(200,200,Qt::KeepAspectRatio));
    ui->logo->setAlignment(Qt::AlignCenter);
    ui->logo->setMargin(50);

    setWindowTitle( tr("User Login") );
    setModal( true );
    connect(manager, SIGNAL(finished(QNetworkReply *)), this, SLOT(replyFinished(QNetworkReply*)));

    // Background
    QPixmap pixmap;
    pixmap.load("://resources/images/Backgorund.png");
    //scaling the image, optional. See the documentation for more options
    pixmap = pixmap.scaled(345,800,Qt::KeepAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Window, pixmap);
    this->setPalette(palette);

    // Button Login
//    QPixmap pixmapButton;
//    pixmapButton.load("://resources/images/LoginEnter.png");
//    //pixmapButton = pixmap.scaled(355,800,Qt::KeepAspectRatio);
//    QPalette paletteButton;
//    paletteButton.setBrush(ui->loginPushButton->backgroundRole(), QBrush(pixmapButton));
//    ui->loginPushButton->setFlat(true);
//    ui->loginPushButton->setAutoFillBackground(true);
//    ui->loginPushButton->setPalette(paletteButton);

    // Button Login
    ui->loginPushButton->setStyleSheet("background-color: rgb(0,39,69); color:white");
    ui->loginPushButton->setMinimumHeight(40);


    // Checkbox
    ui->stayLoggedCheckBox->setStyleSheet("color:white");

    // Input Login
    ui->emailLineEdit->setFixedHeight(30);

    // Input Password
    ui->passwordLineEdit->setFixedHeight(30);

    // CreateAccount
    ui->createAccountPushButton->setStyleSheet("background-color: rgb(0,0,0); color:yellow");
    ui->createAccountPushButton->setMinimumHeight(30);

    // Recover Password
    ui->recoverPasswordPushButton->setStyleSheet("QPushButton{background: transparent; color:white}");







}

LoginDialog::~LoginDialog()
{
    delete ui;
    delete manager;


}

void LoginDialog::on_loginPushButton_clicked()
{
    ui->status->setText("trying to login");

    QNetworkRequest req = QNetworkRequest(QUrl("https://www.docdo.com.br/v3/process/api.php"));
    req.setHeader(req.ContentTypeHeader,"application/x-www-form-urlencoded");

    QString postString = "api=login&login=" + ui->emailLineEdit->text() +"&password="+ui->passwordLineEdit->text();
    manager->post(req,postString.toUtf8());
}

void LoginDialog::replyFinished(QNetworkReply *networkReply)
{
    QString response = networkReply->readAll();
    qDebug() << "Reply:" << response;
    if(response.isEmpty()){
        ui->status->setText("no response from server");
        return;
    }
    QJsonDocument doc = QJsonDocument::fromJson(response.toUtf8());
    if(!doc.isObject())
    {
        ui->status->setText("received invalid response from server");
        return;
    }

    QJsonObject json = doc.object();
    QString error = json["error"].toString();
    if(!error.isEmpty()&&!error.isNull())
    {
        ui->status->setText(error);
        return;
    }

    QJsonObject data = json["data"].toObject();
    if(data.isEmpty())
    {
        ui->status->setText("data response from server is empty");
        return;
    }

    DicomDB::instance()->id = data["id"].toString();
    DicomDB::instance()->login=data["login"].toString();
    DicomDB::instance()->token=data["token"].toString();
    DicomDB::instance()->language=data["language"].toString();
    foreach (QJsonValue cat, data["category"].toArray())
        DicomDB::instance()->category.append(cat.toString());

   // MainWindow *mm = new MainWindow();
    //mm->show();

    CreateExamDialog *cr = new CreateExamDialog();
    cr->show();


    this->close();
}
