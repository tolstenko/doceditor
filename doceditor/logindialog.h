#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QWidget>

#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QComboBox>
#include <QGridLayout>
#include <QStringList>
#include <QDebug>
#include <QString>
#include <QNetworkReply>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QList>

#include "mainwindow.h"

namespace Ui {
class LoginDialog;
}

class LoginDialog : public QDialog
{
 Q_OBJECT

private:
    Ui::LoginDialog *ui;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

public:
    explicit LoginDialog(QWidget * parent = 0);
    ~LoginDialog();

public slots:
    void replyFinished(QNetworkReply *networkReply);
private slots:
    void on_loginPushButton_clicked();

};

#endif // LOGINDIALOG_H
