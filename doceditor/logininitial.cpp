#include "logininitial.h"
#include "ui_logininitial.h"


loginInitial::loginInitial(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::loginInitial)
{
    ui->setupUi(this);


    // Logo DocDo
    QPixmap pix("://resources/images/DocDoLogo.png");
    ui->logo->setPixmap(pix.scaled(200,200,Qt::KeepAspectRatio));
    ui->logo->setAlignment(Qt::AlignCenter);
    //ui->logo->setMargin(50);

    setWindowTitle( tr("Login") );
    setModal( true );
    connect(manager, SIGNAL(finished(QNetworkReply *)), this, SLOT(replyFinished(QNetworkReply*)));

    // Background
    QPixmap pixmap;
    pixmap.load("://resources/images/Backgorund.png");
    //scaling the image, optional. See the documentation for more options
    pixmap = pixmap.scaled(345,800,Qt::KeepAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Window, pixmap);
    this->setPalette(palette);


    // Button Login
    ui->loginPushButtonInitial->setStyleSheet("background-color: rgb(0,39,69); color:white");
    ui->loginPushButtonInitial->setMinimumHeight(40);

    ui->emailInputInitial->setStyleSheet("padding: 0 8px;");


}

loginInitial::~loginInitial()
{
    delete ui;
}

// Check E-mail valid
//bool validaEmail(QString email)
//{
//    bool retorno = true;

//    qDebug() << email;

//    QRegularExpression regex("[0-9a-zA-Z]+([0-9a-zA-Z]*[-._+])*[0-9a-zA-Z]+@[0-9a-zA-Z]+([-.][0-9a-zA-Z]+)*([0-9a-zA-Z]*[.])[a-zA-Z]{2,6}$");

//    if(!regex.match(email).hasMatch())
//    {
//        retorno = false;
//    }

//    qDebug() << "Retorno: " << retorno;

//    return retorno;


//}




void loginInitial::on_loginPushButtonInitial_clicked()
{


    QNetworkRequest req = QNetworkRequest(QUrl("https://www.docdo.com.br/v3/process/api.php"));
    req.setHeader(req.ContentTypeHeader,"application/x-www-form-urlencoded");

    QString postString = "api=checkEmailLogin&login=" + ui->emailInputInitial->text();
    manager->post(req,postString.toUtf8());





    // Check email
//    if(validaEmail(ui->emailInputInitial->text()) == true)
//    {
//        // TODO: Checar se o E-mail existe
//        loginPass->show();
//        this->close();

//    }
//    else
//    {
//        // Set message box
//       msgBox.setText("E-mail " + ui->emailInputInitial->text() + " não encontrado!");

//       if(msgBox.exec() == QMessageBox::Ok)
//       {
//            qDebug() << "OK!";
//            accountNew->show();
//            this->close();

//       }
//       else
//       {
//            qDebug() << "NO!";
//       }

//    }


}




void loginInitial::replyFinished(QNetworkReply *networkReply)
{

    // Message
    QMessageBox msgBox;
    msgBox.setStandardButtons(QMessageBox::Ok);

    msgBox.addButton(QMessageBox::Cancel);

    // Reference screen logindialog.ui
    loginpassword *loginPass = new loginpassword();

    // Reference screen createaccount.ui
    createaccount *accountNew = new createaccount;


    QString response = networkReply->readAll();
    qDebug() << "Reply:" << response;
    if(response.isEmpty()){
        //ui->status->setText("no response from server");
        return;
    }
    QJsonDocument doc = QJsonDocument::fromJson(response.toUtf8());
    if(!doc.isObject())
    {
        //ui->status->setText("received invalid response from server");
        return;
    }

    QJsonObject json = doc.object();
    QString error = json["error"].toString();
    if(!error.isEmpty()&&!error.isNull())
    {
        //ui->status->setText(error);
        return;
    }

    QJsonObject data = json["data"].toObject();
    if(data.isEmpty())
    {
        //ui->status->setText("data response from server is empty");

        msgBox.setText("E-mail " + ui->emailInputInitial->text() + " não encontrado!");

       if(msgBox.exec() == QMessageBox::Ok)
       {
            qDebug() << "OK!";
            accountNew->show();
            this->close();
       }

    }
    else
    {
        DicomDB::instance()->login = ui->emailInputInitial->text();
        qDebug() << "EMAIL " << DicomDB::instance()->login;
        loginPass->show();
        this->close();
    }

//    DicomDB::instance()->id = data["id"].toString();
//    DicomDB::instance()->login=data["login"].toString();
//    DicomDB::instance()->token=data["token"].toString();
//    DicomDB::instance()->language=data["language"].toString();
//    foreach (QJsonValue cat, data["category"].toArray())
//        DicomDB::instance()->category.append(cat.toString());



}
