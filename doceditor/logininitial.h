#ifndef LOGININITIAL_H
#define LOGININITIAL_H

#include <QDialog>

#include <QProgressDialog>
#include <QMessageBox>

#include <QLabel>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QComboBox>
#include <QGridLayout>
#include <QStringList>
#include <QDebug>
#include <QString>
#include <QNetworkReply>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QList>

#include "logindialog.h"
#include "createaccount.h"
#include "loginpassword.h"
#include "dicomdb.h"

namespace Ui {
class loginInitial;
}

class loginInitial : public QDialog
{
    Q_OBJECT

public:
    explicit loginInitial(QWidget *parent = 0);
    ~loginInitial();

public slots:
    void replyFinished(QNetworkReply *networkReply);
private slots:

    void on_loginPushButtonInitial_clicked();

private:
    Ui::loginInitial *ui;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
};

#endif // LOGININITIAL_H
