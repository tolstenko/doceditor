#include "loginpassword.h"
#include "ui_loginpassword.h"


loginpassword::loginpassword(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::loginpassword)
{
    ui->setupUi(this);



    // Logo DocDo
    QPixmap pix("://resources/images/DocDoLogo.png");
    ui->logo->setPixmap(pix.scaled(200,200,Qt::KeepAspectRatio));
    ui->logo->setAlignment(Qt::AlignCenter);
    //ui->logo->setMargin(50);

    setWindowTitle( tr("Password") );
    //setModal( true );
    connect(manager2, SIGNAL(finished(QNetworkReply *)), this, SLOT(replyFinished2(QNetworkReply*)));

    // Background
    QPixmap pixmap;
    pixmap.load("://resources/images/Backgorund.png");
    //scaling the image, optional. See the documentation for more options
    pixmap = pixmap.scaled(345,800,Qt::KeepAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Window, pixmap);
    this->setPalette(palette);


    // Button Login
    ui->loginPushButtonPassword->setStyleSheet("background-color: rgb(0,39,69); color:white");
    ui->loginPushButtonPassword->setMinimumHeight(40);

    ui->passwordInputInitial->setStyleSheet("padding: 0 8px;");



}

loginpassword::~loginpassword()
{
    delete ui;
}

void loginpassword::on_loginPushButtonPassword_clicked()
{

    //ui->status->setText("trying to login");

    QNetworkRequest req = QNetworkRequest(QUrl("https://www.docdo.com.br/v3/process/api.php"));
    req.setHeader(req.ContentTypeHeader,"application/x-www-form-urlencoded");

    QString postString = "api=login&login=" + DicomDB::instance()->login +"&password="+ui->passwordInputInitial->text();
    manager2->post(req,postString.toUtf8());

}

void loginpassword::replyFinished2(QNetworkReply *networkReply)
{
    QString response = networkReply->readAll();
    qDebug() << "Reply:" << response;
    if(response.isEmpty()){
        //ui->status->setText("no response from server");
        return;
    }
    QJsonDocument doc = QJsonDocument::fromJson(response.toUtf8());
    if(!doc.isObject())
    {
        //ui->status->setText("received invalid response from server");
        return;
    }

    QJsonObject json = doc.object();
    QString error = json["error"].toString();
    if(!error.isEmpty()&&!error.isNull())
    {
        //ui->status->setText(error);
        return;
    }

    QJsonObject data = json["data"].toObject();
    if(data.isEmpty())
    {
        //ui->status->setText("data response from server is empty");
        return;
    }

    DicomDB::instance()->id = data["id"].toString();
    DicomDB::instance()->login=data["login"].toString();
    DicomDB::instance()->token=data["token"].toString();
    DicomDB::instance()->language=data["language"].toString();
    foreach (QJsonValue cat, data["category"].toArray())
        DicomDB::instance()->category.append(cat.toString());


    CreateExamDialog *cr = new CreateExamDialog();
    cr->show();


    this->close();
}
