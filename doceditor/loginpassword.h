#ifndef LOGINPASSWORD_H
#define LOGINPASSWORD_H

#include <QWidget>

#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QComboBox>
#include <QGridLayout>
#include <QStringList>
#include <QDebug>
#include <QString>
#include <QNetworkReply>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QList>

#include "createexamdialog.h"
#include "logininitial.h"
#include "dicomdb.h"

namespace Ui {
class loginpassword;
}

class loginpassword : public QWidget
{
    Q_OBJECT

public:
    explicit loginpassword(QWidget *parent = 0);
    ~loginpassword();


public slots:
    void replyFinished2(QNetworkReply *networkReply);
private slots:
    void on_loginPushButtonPassword_clicked();

private:
    Ui::loginpassword *ui;
    QNetworkAccessManager *manager2 = new QNetworkAccessManager(this);
};

#endif // LOGINPASSWORD_H
