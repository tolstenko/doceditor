#include "mainwindow.h"
#include "windowmanager.h"

#include <QApplication>
#include <QSplashScreen>
#include <QTimer>
#include <QSettings>
#include <QString>
#include <QtDebug>
#include <QDir>

#include "uploaderwindow.h"
#include "createexamdialog.h"
#include "progresswindow.h"
#include "logininitial.h"
#include "uploaderfiles.h"

int main(int argc, char *argv[])
{
    // app startup
    QApplication app(argc, argv);

    // default last opened folder
    const QString DEFAULT_DIR_KEY("default_dir");
    QSettings settings(QSettings::NativeFormat,QSettings::UserScope,"InfiniBrains","InfiniEditor");

    // configure settings path
    if(settings.value(DEFAULT_DIR_KEY).toString().isEmpty())
        settings.setValue(QString(DEFAULT_DIR_KEY),QDir::homePath());

    QSplashScreen * splash = new QSplashScreen;
    splash->setPixmap(QPixmap(":/resources/images/infini.png")); //://resources/images/docdo.png
    splash->show();

    splash->showMessage( "Loading Modules..." );
    app.processEvents();

    QTimer::singleShot(2000,splash,SLOT(close()));

//    loginInitial ld;
//    QTimer::singleShot(2000,&ld,SLOT(exec()));

    uploaderfiles up;
    QTimer::singleShot(2000,&up,SLOT(show()));


    //MainWindow w;
    //QTimer::singleShot(2000,&w,SLOT(show()));


     //CreateExamDialog ex;
     //ex.show();
    //UploaderWindow up;
    //up.show();

    //progresswindow p;
   // p.show();

    return app.exec();
}
