#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}

DicomBrowser *d;
void MainWindow::on_actionDicomBrowser_triggered()
{ 


    if(d==NULL){
        d = new DicomBrowser(this->parentWidget());
    d->showNormal();

    }
}
