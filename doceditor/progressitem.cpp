#include "progressitem.h"

progressitem::progressitem(QString text, QWidget* parent):
    QWidget(parent)
{


    setStyleSheet(".QWidget{border-bottom: 1px solid lightgrey;}");
            layout = new QHBoxLayout(this);
            layout->setSpacing(12);
            progress = new progressbar();
            progress->setFixedSize(70, 70);
            layout->addWidget(progress);
            label = new QLabel(text);
            QFont f( "Arial", 13, QFont::Bold);
            label->setFont(f);
            layout->addWidget(label, 1);


            progressCircleAnimation = new QPropertyAnimation(progress, "outerRadius", progress);
            progressCircleAnimation->setDuration(750);
            progressCircleAnimation->setEasingCurve(QEasingCurve::OutQuad);
            progressCircleAnimation->setStartValue(0.0);
            progressCircleAnimation->setEndValue(1.0);
            progressCircleAnimation->start(QAbstractAnimation::DeleteWhenStopped);
}

