#ifndef PROGRESSITEM_H
#define PROGRESSITEM_H
#pragma once
#include <QLabel>
#include <QBoxLayout>
#include <QPushButton>
#include <QPushButton>
#include <QTimer>
#include "progressbar.h"

#include <QWidget>

class progressitem: public QWidget
{
    Q_OBJECT
public:
    explicit progressitem(QString text, QWidget *parent = 0);
    void finish(bool success);
    progressbar * progress;

private:
    QLabel * label;
    QHBoxLayout * layout;
     QPushButton * closeButton;
     QPropertyAnimation * progressCircleAnimation;
};

#endif // PROGRESSITEM_H
