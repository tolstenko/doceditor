#ifndef PROGRESSWINDOW_H
#define PROGRESSWINDOW_H

#include <QMainWindow>

namespace Ui {
class progresswindow;
}

class progresswindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit progresswindow(QWidget *parent = 0);
    ~progresswindow();

public slots:
    void addIndefinite();



private:
    Ui::progresswindow *ui;
};

#endif // PROGRESSWINDOW_H
