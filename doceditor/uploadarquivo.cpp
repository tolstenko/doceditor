#include "uploadarquivo.h"
#include "ui_uploadarquivo.h"


UploadArquivo::UploadArquivo(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::UploadArquivo)
{
    ui->setupUi(this);
    ui->comboBox->addItem("exams");
    ui->comboBox->addItem("reports");
    connect(manager, SIGNAL(finished(QNetworkReply *)), this, SLOT(reply(QNetworkReply*)));
}

UploadArquivo::~UploadArquivo()
{
    delete ui;
}

void UploadArquivo::on_selecionarpasta_clicked()
{
    const QString DEFAULT_DIR_KEY("default dir");
    QSettings settings(QSettings::NativeFormat,QSettings::UserScope,"InfiniBrains","InfiniEditor");
    QString dir = QFileDialog::getExistingDirectory(this,tr("Open Directory"),
                                                    settings.value(DEFAULT_DIR_KEY).toString(),
                                                    QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    if(!dir.isEmpty())
    {
        settings.setValue(DEFAULT_DIR_KEY,dir);

        qDebug()<<"updating db";
        DicomDB::instance()->navigate(dir);

        qDebug()<<"compressing";
        compress(dir);

        ui->lineEdit->setText(dir);


    }
    else
        return;

}


// Upload Completed
void UploadArquivo::setExamUploadCompleted(){

   QNetworkRequest req = QNetworkRequest(QUrl("https://www.docdo.com.br/v3/process/api.php"));
   req.setHeader(req.ContentTypeHeader,"application/x-www-form-urlencoded");

   QString postString = "api=finishedupload&exame_id="+DicomDB::instance()->exam_id+"&token="+DicomDB::instance()->token;
   manager->post(req,postString.toUtf8());

   p.show();

   QEventLoop loop;
   QMetaObject::Connection conect = loop.connect(manager, SIGNAL(finished(QNetworkReply*)),SLOT(quit()));

   loop.exec();
   loop.disconnect(conect);
   p.close();
   this->close();


}

void UploadArquivo::on_replySignS3()
{

    QString responsedata = QString(replySignS3->readAll());
    qDebug() << "read s3" << responsedata;

    QString responseSignS3 = responsedata;
    if(responseSignS3.isEmpty()){
       ui->status->setText("no response from server");
       return;
    }
    QJsonDocument doc = QJsonDocument::fromJson(responseSignS3.toUtf8());
    if(!doc.isObject()){
        ui->status->setText("received invalid response from server");
        return;
    }
    QJsonObject json = doc.object();
    QString error = json["error"].toString();
    if(!error.isEmpty()&&!error.isNull()){
       ui->status->setText(error);
       return;
    }
    QJsonObject data = json["data"].toObject();
    if(data.isEmpty()){
      ui->status->setText("data response from server is empty");
      return;
    }

    qDebug() << "URL REQUEST" << data["signed_request"].toString();

    presignedUrl = data["signed_request"].toString();
    fileId = data["file_id"].toString();

}


void UploadArquivo::on_pushButton_clicked()
{
    QString filepath = parent.path()+"/compressed.zip";
    QFile file(filepath);
    if (!file.open(QIODevice::ReadOnly))
        return;

    QByteArray blob = file.readAll();
    QString h = hash(blob);
    file.close();

    QNetworkRequest req = QNetworkRequest(QUrl("https://www.docdo.com.br/v3/process/api.php"));
    req.setHeader(req.ContentTypeHeader,"application/x-www-form-urlencoded");
    QString pa = parent.path()+"/compressed.zip";
    QString postString = "api=sign_s3&file_name="+pa+"&type="+ui->comboBox->currentText()+"&paciente_id="+DicomDB::instance()->id+"&exame_id="+DicomDB::instance()->exam_id+"&study=WB"+"&sha1="+h+"&token="+DicomDB::instance()->token+"&sid=0";

    replySignS3 = manager->post(req,postString.toUtf8());

    QEventLoop loop;
    QMetaObject::Connection con = loop.connect(manager, SIGNAL(finished(QNetworkReply*)),SLOT(quit()));
    loop.exec();
    loop.disconnect(con);


    QString responsedata = QString(replySignS3->readAll());
    qDebug() << "read s3" << responsedata;

    QString responseSignS3 = responsedata;
    if(responseSignS3.isEmpty()){
       ui->status->setText("no response from server");
       return;
    }
    QJsonDocument doc = QJsonDocument::fromJson(responseSignS3.toUtf8());
    if(!doc.isObject()){
        ui->status->setText("received invalid response from server");
        return;
    }
    QJsonObject json = doc.object();
    QString error = json["error"].toString();
    if(!error.isEmpty()&&!error.isNull()){
       ui->status->setText(error);
       return;
    }
    QJsonObject data = json["data"].toObject();
    if(data.isEmpty()){
      ui->status->setText("data response from server is empty");
      return;
    }

    qDebug() << "JSON" << responseSignS3;
    qDebug() << "SIGN_REQUEST" << data["signed_request"].toString();
    qDebug() << "URL" << data["url"].toString();
    qDebug() << "ID" << data["file_id"].toString();

    presignedUrl = data["signed_request"].toString();
    fileId = data["file_id"].toString();



    // Enviando arquivo para o servidor
    QNetworkRequest request(presignedUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, QString("application/zip"));
    QNetworkReply* reply = manager->put(request, blob);


    con = loop.connect(manager, SIGNAL(finished(QNetworkReply*)),SLOT(quit()));
    QMetaObject::Connection progresscon= connect(reply, SIGNAL(uploadProgress(qint64, qint64)), SLOT(progressChanged(qint64,qint64)));
    loop.exec();
    loop.disconnect(con);
    disconnect(progresscon);


    // fazer segundo post para API VALIDATEFILE
    QNetworkRequest req2 = QNetworkRequest(QUrl("https://www.docdo.com.br/v3/process/api.php"));
    req2.setHeader(req2.ContentTypeHeader,"application/x-www-form-urlencoded");
    QString postString2 = "api=validatefile&type="+ui->comboBox->currentText()+"&file_id="+fileId+"&exams_id="+DicomDB::instance()->exam_id+"&token="+DicomDB::instance()->token+"&sha1="+h+"&sid=0";

    qDebug() << "STRING" << postString2;

    replyValidadeFile = manager->post(req2,postString2.toUtf8());

    // espera pelo post
    con = loop.connect(manager, SIGNAL(finished(QNetworkReply*)),SLOT(quit()));
    loop.exec();
    loop.disconnect(con);

    qDebug() << "VALIDATE" << replyValidadeFile->readAll();

    setExamUploadCompleted();
}

// funcao que retorna o hash de um array de bytes de um arquivo
QString UploadArquivo::hash(QByteArray dataToBeHashed)
{
    QCryptographicHash h(QCryptographicHash::Sha1);
    h.addData(dataToBeHashed);
    return h.result().toHex();
}

// ProgressBar Upload Exam
void UploadArquivo::progressChanged(qint64 a, qint64 b)
{
    if (b > 0)
    {
        qDebug() << "Uploading " << a  << "/" << b << "%" << (double)a/(double)b*100.0;
        ui->progressBarUpload->setValue((a*100/b));
        qApp->processEvents();

         ui->txtLoadingUploadExam->setText(QString::number((double)a/(double)b*100.0,'f',2) +"%");
    }


}

// ProgressBar Upload File Path
void UploadArquivo::progressChangedFilePath(qint64 a, qint64 b)
{
    if (b > 0)
    {
        qDebug() << "Uploading " << a  << "/" << b << "%" << (double)a/(double)b*100.0;
        ui->progressBarFilePath->setValue((a*100/b));
        qApp->processEvents();

         //ui->txtLoadingUploadExam->setText(QString::number((double)a/(double)b*100.0,'f',2) +"%");
    }

}


QDataStream dataStream;

// FOLDER COMPRESS
bool UploadArquivo::compressFolder(QString sourceFolder, QString destinationFile)
{
    QDir src(sourceFolder);
    if(!src.exists())//folder not found
    {
        return false;
    }

    QFile file;
    file.setFileName(destinationFile);
    if(!file.open(QIODevice::WriteOnly))//could not open file
    {
        return false;
    }

    dataStream.setDevice(&file);

    bool success = compress(sourceFolder);
    file.close();

    return success;
}


QFileInfoList filesList;
QDir dir;


void spin()
{

    QDir up = dir;
    up.cdUp();

    QStringList files;
    for(auto file :filesList )
        files.append(file.absoluteFilePath());

    //JlCompress::compressFiles(up.absolutePath() + "/compressed.zip",files);


}


bool UploadArquivo::compress(QString sourceFolder)
{
    dir = QDir(sourceFolder);
    if(!dir.exists())
        return false;

    QMimeDatabase mimedb;
    QStringList mimeTypeFilters;

    mimeTypeFilters << "image/jpeg" << "image/pjpeg" << "image/png" << "image/gif"
                    << "image/x-portable-bitmap" << "image/x-portable-graymap"
                    << "application/dicom";

    filesList.clear();
    // while there is new file, fill file list

    // file directory iterator
    QDirIterator it(dir.absolutePath(), QStringList() << "*", QDir::Files, QDirIterator::Subdirectories);
    while (it.hasNext())
    {
        QString path = it.next();

        // check mime type
        QMimeType mime = mimedb.mimeTypeForFile(path);

        // check if it is supported
        if(!mimeTypeFilters.contains(mime.name()))
        {
            qDebug()<<mime.name() << path;
            continue;
        }
        filesList.append(path);
    }


    // Create a progress dialog.
    QProgressDialog dialog;
    dialog.setLabelText(QString("Progressing..."));


    // Create a QFutureWatcher and connect signals and slots.
    QFutureWatcher<void> futureWatcher;

    QObject::connect(&futureWatcher, SIGNAL(finished()), &dialog, SLOT(reset()));
    QObject::connect(&dialog, SIGNAL(canceled()), &futureWatcher, SLOT(cancel()));
    QObject::connect(&futureWatcher, SIGNAL(progressRangeChanged(int,int)), &dialog, SLOT(setRange(int,int)));
    QObject::connect(&futureWatcher, SIGNAL(progressValueChanged(int)), &dialog, SLOT(setValue(int)));

    // Start the computation.
    QFuture<void> future = QtConcurrent::run(spin);

    futureWatcher.setFuture(future);

    // Display the dialog and start the event loop.

    dialog.exec();

    future.waitForFinished();

    if(futureWatcher.isCanceled())
    {
        qDebug() << "canceled!";
        QMessageBox::critical(this, "Canceled", "You cliked cancel!");
    }
    else
    {
        qDebug() << "finished!";
        QMessageBox::critical(this, "finished", "Exam uploaded successfully!");
    }

    return true;
}


bool UploadArquivo::decompressFolder(QString sourceFile, QString destinationFolder)
{
    //validation
    QFile src(sourceFile);
    if(!src.exists())
    {//file not found, to handle later
        return false;
    }
    QDir dir;
    if(!dir.mkpath(destinationFolder))
    {//could not create folder
        return false;
    }

    QFile file;
    file.setFileName(sourceFile);
    if(!file.open(QIODevice::ReadOnly))
        return false;

    dataStream.setDevice(&file);

    while(!dataStream.atEnd())
    {
        QString fileName;
        QByteArray data;

        //extract file name and data in order
        dataStream >> fileName >> data;

        //create any needed folder
        QString subfolder;
        for(int i=fileName.length()-1; i>0; i--)
        {
            if((QString(fileName.at(i)) == QString("\\")) || (QString(fileName.at(i)) == QString("/")))
            {
                subfolder = fileName.left(i);
                dir.mkpath(destinationFolder+"/"+subfolder);
                break;
            }
        }

        QFile outFile(destinationFolder+"/"+fileName);
        if(!outFile.open(QIODevice::WriteOnly))
        {
            file.close();
            return false;
        }
        outFile.write(qUncompress(data));
        outFile.close();
    }

    file.close();
    return true;
}
