#ifndef UPLOADARQUIVO_H
#define UPLOADARQUIVO_H


#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFileDialog>
#include <QSettings>
#include <QDebug>
#include <QCryptographicHash>
#include <QEventLoop>
#include <QtCore>
#include <QMessageBox>
#include <QDialog>
#include <QProgressDialog>
#include <QThread>
#include <QFutureWatcher>
#include <QtConcurrent>
#include <QtWidgets>
#include <QDirIterator>
#include <QFile>
#include <QObject>
#include <QDir>
#include <QDataStream>

#include "progresswindow.h"
#include "foldercompressor.h"
#include "dicomdb.h"
//#include "quazip/JlCompress.h"

namespace Ui {
class UploadArquivo;
}

class UploadArquivo : public QMainWindow
{
    Q_OBJECT

public:
    explicit UploadArquivo(QWidget *parent = 0);
    ~UploadArquivo();
    QDir parent;
    bool compressFolder(QString sourceFolder, QString destinationFile);
    //bool compress(QString sourceFolder, QString prefex);
    bool decompressFolder(QString sourceFile, QString destinationFolder);
    void setExamUploadCompleted();
    QString hash(QByteArray dataToBeHashed);
    progresswindow p;
    QNetworkReply *replySignS3, *replyValidadeFile, *replyUpload;
    QString presignedUrl;
    QString fileId;


private slots:
    void on_selecionarpasta_clicked();
    void progressChanged(qint64, qint64);
    void progressChangedFilePath(qint64,qint64);
    //void reply(QNetworkReply *networkReply);
    void on_pushButton_clicked();
    void on_replySignS3();

private:
    Ui::UploadArquivo *ui;

    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    bool compress(QString sourceFolder);


};

#endif // UPLOADARQUIVO_H
