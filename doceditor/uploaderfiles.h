#ifndef UPLOADERFILES_H
#define UPLOADERFILES_H

#include <QWidget>

namespace Ui {
class uploaderfiles;
}

class uploaderfiles : public QWidget
{
    Q_OBJECT

public:
    explicit uploaderfiles(QWidget *parent = 0);
    ~uploaderfiles();

private:
    Ui::uploaderfiles *ui;
};

#endif // UPLOADERFILES_H
