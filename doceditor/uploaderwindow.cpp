#include "uploaderwindow.h"
#include "ui_uploaderwindow.h"
#include <QString>
#include<QItemSelectionModel>
#include <QMouseEvent>



#include <iostream>
#include <stdio.h>

UploaderWindow::UploaderWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::UploaderWindow)
{
    ui->setupUi(this);
    connect(ui->selectFolder,SIGNAL(released()),this,SLOT(on_scandirectory_clicked()));
    patientsmodel = new QSqlQueryModel();
    studiesmodel = new  QSqlQueryModel();
    seriesmodel = new QSqlQueryModel();
    patientsquery = new QSqlQuery(DicomDB::instance()->db);
    studiesquery = new QSqlQuery(DicomDB::instance()->db);
    seriesquery = new QSqlQuery(DicomDB::instance()->db);
    loadquery = new QSqlQuery(DicomDB::instance()->db);
    UpdateListPatients();
}

UploaderWindow::~UploaderWindow()
{
    delete ui;
}

void UploaderWindow::on_scandirectory_clicked()
{
    const QString DEFAULT_DIR_KEY("default_dir");
       QSettings settings(QSettings::NativeFormat,QSettings::UserScope,"InfiniBrains","InfiniEditor");

       QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                     settings.value(DEFAULT_DIR_KEY).toString(),
                     QFileDialog::ShowDirsOnly
                     | QFileDialog::DontResolveSymlinks);

       // navigate through folders recursivelly if dir is set
       if(!dir.isEmpty())
       {
           settings.setValue(DEFAULT_DIR_KEY,dir);
           DicomDB::instance()->navigate(dir);

           //FolderCompressor *compressor = new FolderCompressor();
          // QDir parent = QDir(dir);
           //parent.cdUp();
          // compressor->compressFolder(dir,parent.path()+"/compressed.zip");
          // delete compressor;
       }
       else
           return;
this->UpdateListPatients();
}

void UploaderWindow::UpdateListSeries()
{
  seriesquery->prepare("SELECT DISTINCT SeriesNumber FROM dicoms ");
  seriesquery->exec();
  seriesmodel->setQuery(*seriesquery);
  ui->seriesTableView->verticalHeader()->hide();
  ui->seriesTableView->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
  ui->seriesTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
  ui->seriesTableView->setModel(seriesmodel);
  UpdateListStudies();
}

void UploaderWindow::UpdateListStudies()
{

    studiesquery->prepare("SELECT DISTINCT StudyDescription FROM dicoms");
    studiesquery->exec();
    studiesmodel->setQuery(*studiesquery);
    ui->studiesTableView->verticalHeader()->hide();
    ui->studiesTableView->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    ui->studiesTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->studiesTableView->setModel(studiesmodel);

}
void UploaderWindow::UpdateListPatients()
{

    QModelIndex selectmodel;
    patientsquery->prepare("SELECT DISTINCT PatientsName FROM dicoms");
    patientsquery->exec();
    patientsmodel->setQuery(*patientsquery);
    ui->nameTableView->verticalHeader()->hide();
    ui->nameTableView->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    ui->nameTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->nameTableView->setModel(patientsmodel);
    selectmodel=ui->nameTableView->currentIndex();
    qDebug()<<ui->nameTableView->itemDelegateForRow(2);
    UpdateListSeries();


}
