#ifndef UPLOADERWINDOW_H
#define UPLOADERWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QFileDialog>
#include <QDirIterator>
#include <QStringList>
#include <QImage>
#include <QLabel>
#include <QSettings>
#include <QWidgetList>
#include <QStringList>
#include <QVector>



#include "dicomdb.h"
#include "gdcmImageReader.h"
#include "gdcmStringFilter.h"
#include "gdcmStrictScanner.h"
#include "gdcmSimpleSubjectWatcher.h"
#include "gdcmFileNameEvent.h"
#include "gdcmSorter.h"
#include "gdcmIPPSorter.h"
#include "gdcmScanner.h"
#include "gdcmDataSet.h"
#include "gdcmAttribute.h"
#include "gdcmTesting.h"

#include "foldercompressor.h"

namespace Ui {
class UploaderWindow;
}

class UploaderWindow : public QMainWindow
{
    Q_OBJECT
public slots:
    void on_scandirectory_clicked();


public:
    explicit UploaderWindow(QWidget *parent = 0);
    ~UploaderWindow();

private:
    Ui::UploaderWindow *ui;
    void UpdateListPatients();
        void UpdateListStudies();
        void UpdateListSeries();
        QSqlQueryModel * patientsmodel;
          QSqlQueryModel * studiesmodel;
          QSqlQueryModel * seriesmodel;
          QSqlQuery * patientsquery;
          QSqlQuery * studiesquery;
          QSqlQuery * seriesquery;
           QSqlQuery * loadquery;

};

#endif // UPLOADERWINDOW_H
