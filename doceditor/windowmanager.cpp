#include "windowmanager.h"
#include <QSplashScreen>
#include <QPixmap>
#include <QApplication>
#include <QTimer>

WindowManager::WindowManager()
{

}

void WindowManager::ShowSplashScreen(QApplication *app)
{
    QSplashScreen * splash = new QSplashScreen;
    splash->setPixmap(QPixmap(":/resources/images/infini.png"));
    splash->show();

    splash->showMessage( "Loading Modules..." );
    app->processEvents();


    splash->close();
    delete splash;
}
