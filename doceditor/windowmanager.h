#ifndef WINDOWMANAGER_H
#define WINDOWMANAGER_H

#include <QApplication>

class WindowManager
{
public:
  static WindowManager& instance()
  {
      // Lazy initialize.
      if (instance_ == nullptr) instance_ = new WindowManager();
      return *instance_;
  }
  ~WindowManager(){}

  void ShowSplashScreen(QApplication*);

protected:
  WindowManager();
  static WindowManager* instance_;
};

#endif // WINDOWMANAGER_H

